## BH Coding Excercise 3s
### Introduction
You need to provide a link to a GitHub repo containing an object-oriented solution worked in
Perl. Commit groups of related changes as you make them rather than making one single
commit at the end of the project. In addition to the provided solution, a file containing the list of
any assumptions and/or to-do items should be included.

Work on the exercise is time-boxed to 90 minutes.

You may not have time to provide a solution that meets the full set of requirements. Prioritize
completion of a working, tested, extensible, well-designed vertical slice over a complete
solution.

Data persistence between runs of the service is not necessary.

### Background
You have been contracted to create a prototype service for scheduling and tracking
transaction-based email. This will eventually be expanded to be a more complete subscription
management service but for now will only schedule/track email to be sent.

## Prototype considerations
* This repo includes the service and a small frontend to trigger the service calls
* All service responses are in JSON format
* Data storage is session based.
* Data is validated for completeness (if required fields are not empty) not if it's correct (does not validate data types or dates)
  * StartDt (startDt) needs to be string with the format 'm/d/Y', ie: 01/12/2021
  * Contract Length (length) needs to be a positive integer
* As requested this is a prototype, not production ready.
  What is done here can easily be ported to a more robust and expandable/maintainable solution with minimal effort (see TODO section below)

#### Build/startup
There's only one container for php+apache so:
> ...$ docker-compose up

to build/startup the app.
* Service will listen on port 9000 (just in case you have something else running on your 80)
* Services access points are:
    - http://localhost:9000/web/ : small UI to trigger the api calls
    - http://localhost:9000/api/ : api calls.

### TODO
* Move everything to a framework: Yii2, Laravel, etc
    * Implement models for: Customer, Product, CustomerProduct entities
    * Implement persistent data storage: MySQL/Postgres
    * Implement API authentication/authorization
    * Implement API throttling (rate-limit)
    * Add caching for commonly used data: ie: Products
    * Add Logging
    * Add TESTS!!
    * Enable CORS check (if required)
    * Allow json or xml api responses
* Refactor docker infrastructure to:
    * PHP-FPM container for the service, prefer -alpine flavors
    * Nginx/Apache as proxy to forward requests to php-fpm container
    * MySQL/Postgres container for storage

 