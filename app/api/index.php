<?php
require_once 'IndexController.php';
session_start();

$method = $_SERVER['REQUEST_METHOD'];
parse_str($_SERVER['QUERY_STRING'], $qrystring);

$qrystring = array_keys($qrystring);
$action = (count($qrystring) > 0) ? $qrystring[0] : null;

$payload = [];
switch ($method) {
    case 'POST':
        $payload = &$_POST;
        break;
    case 'GET':
        $payload = &$_GET;
        break;
    default:
        throw new HttpRequestException('Unsupported method');
}

$index = new IndexController($action, $payload);
$response = $index->execute();

header('Content-Type: application/json');
echo json_encode($response);