<?php

require_once 'CustomerProductDataSource.php';
require_once 'BaseRequest.php';
require_once 'BaseResponse.php';

class IndexController
{
    private string $action = '';
    private BaseRequest $payload;
    private array $allowedActions = ['addProduct', 'removeProduct', 'getAll', 'listEmails'];
    private array $validationErrors = [];

    public function __construct($action, $payload)
    {
        $this->action = $action;
        $this->payload = new BaseRequest($payload);
        $this->validationErrors = [];
    }

    public function execute(): array
    {
        $response = new BaseResponse();
        if (!in_array($this->action, $this->allowedActions)) {
            http_response_code(404);
            $response->success = false;
            $response->msg = 'Invalid API Request';
        } else {
            try {
                switch ($this->action) {
                    case 'addProduct':
                        $response = $this->addProduct();
                        break;
                    case 'getAll':
                        $response = $this->getAll();
                        break;
                    case 'removeProduct':
                        $response = $this->removeProduct();
                        break;
                    case 'listEmails':
                        $response = $this->listEmails();
                        break;
                }
            } catch (Exception $e) {
                $response->success = false;
                $response->msg = $e->getMessage();
            }
        }
        return $response->toArray();
    }

    private function addProduct(): BaseResponse
    {
        $response = new BaseResponse();
        if (!$this->validateRequest()) {
            $response->success = false;
            $response->msg = 'Product could not be added, Errors: ' . join(',', $this->validationErrors);
            $response->data = $this->validationErrors;
        }
        $ds = new CustomerProductDataSource();
        $ds->insertOrUpdate($this->payload);
        $response->success = true;
        $response->msg = 'Product inserted successfully';
        return $response;
    }

    private function removeProduct(): BaseResponse
    {
        $response = new BaseResponse();
        if (!$this->validateRequest()) {
            $response->success = false;
            $response->msg = 'Product could not be deleted, Errors: ' . join(',', $this->validationErrors);
            $response->data = $this->validationErrors;
        } else {
            $ds = new CustomerProductDataSource();
            try {
                $ds->delete($this->payload);
                $response->success = true;
                $response->msg = 'Product deleted successfully';
            } catch (Exception $e) {
                $response->success = false;
                $response->msg = 'Product could not deleted: ' . $e->getMessage();
            }
        }
        return $response;
    }

    private function getAll(): BaseResponse
    {
        $response = new BaseResponse();
        $ds = new CustomerProductDataSource();
        $response->success = true;
        $response->data = $ds->getAll();
        return $response;
    }

    private function listEmails(): BaseResponse
    {
        $response = new BaseResponse();
        $ds = new CustomerProductDataSource();
        $custProd = $ds->getAll();
        $emails = [];
        try {
            foreach ($custProd as $row) {
                $startDt = new DateTime($row[3]);
                $expireDt = (new DateTime($row[3]))->add(new DateInterval('P' . $row[4] . 'M'));
                $emailDates = [];
                switch ($row[1]) {
                    case 'domain':
                        $emailDates[] = $expireDt->sub(new DateInterval('P2D'))->format('Y-m-d');
                        break;
                    case 'hosting':
                        $emailDates[] = $startDt->add(new DateInterval('P1D'))->format('Y-m-d');
                        $emailDates[] = $expireDt->sub(new DateInterval('P3D'))->format('Y-m-d');
                        break;
                    case 'pdomain':
                        $emailDates[] = $expireDt->sub(new DateInterval('P2D'))->format('Y-m-d');
                        $emailDates[] = $expireDt->sub(new DateInterval('P9D'))->format('Y-m-d');
                        break;
                }
                foreach ($emailDates as $date) {
                    $emails[] = [
                        'custId' => $row[0],
                        'prodName' => $row[1],
                        'domain' => $row[2],
                        'emailDt' => $date
                    ];
                }
                uasort($emails, function ($a, $b) {
                    $adate = new DateTime($a['emailDt']);
                    $bdate = new DateTime($b['emailDt']);
                    if ($adate == $bdate) return 0;
                    return $adate < $bdate ? -1 : 1;
                });
            }
            $response->success = true;
            $response->data = array_values($emails);
        } catch (\Exception $e) {
            $response->success = false;
            $response->msg = $e->getMessage();
            $response->data = [];
        }
        return $response;
    }

    private function validateRequest(): bool
    {
        $this->validationErrors = [];
        if (empty($this->payload->custId)) {
            $this->validationErrors[] = 'Customer Id is required';
        }
        if (empty($this->payload->prodName)) {
            $this->validationErrors[] = 'Product Name is required';
        }
        if (empty($this->payload->domain)) {
            $this->validationErrors[] = 'Domain Name is required';
        }
        if ($this->action === 'addProduct' && empty($this->payload->startDt)) {
            $this->validationErrors[] = 'Start Date is required';
        }
        if ($this->action === 'addProduct' && empty($this->payload->length)) {
            $this->validationErrors[] = 'Contract length is required';
        }
        return (count($this->validationErrors) === 0);
    }

}