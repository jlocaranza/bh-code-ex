<?php


class BaseResponse
{
    public bool $success;
    public string $msg;
    public array $data;

    public function __construct($success=false, $msg='', $data=[])
    {
        $this->success = $success;
        $this->msg = $msg;
        $this->data = $data;
    }


    public function toArray(): array
    {
        return [
            'success' => $this->success,
            'msg' => $this->msg,
            'data' => $this->data,
        ];
    }
}