<?php
require_once 'BaseRequest.php';

class CustomerProductDataSource
{
    private array $dataset = [];

    public function __construct()
    {
        if (!isset($_SESSION['customerProducts'])) {
            $_SESSION['customerProducts'] = [];
        }
        $this->dataset = array_values($_SESSION['customerProducts']);
    }

    public function insertOrUpdate(BaseRequest $payload)
    {
        $index = $this->exists($payload->custId, $payload->prodName, $payload->domain);
        if (!$index) {
            $this->dataset[] = $payload->toArray();
        } else {
            $this->dataset[$index] = $payload->toArray();
        }
        $this->saveAll();
    }

    public function delete(BaseRequest $payload)
    {
        $index = $this->exists($payload->custId, $payload->prodName, $payload->domain);
        if ($index===false) {
            throw new Exception("Record not found");
        }
        unset($this->dataset[$index]);
        $this->saveAll();
        return true;
    }

    public function getAll(): array
    {
        return $this->dataset;
    }

    private function exists($custId, $prodName, $domain)
    {
        for ($i = 0; $i < count($this->dataset); $i++) {
            $row = $this->dataset[$i];
            if ($row[0] === $custId && $row[1] === $prodName && $row[2] === $domain) {
                return $i;
            }
        }
        return false;
    }

    private function saveAll()
    {
        $_SESSION['customerProducts'] = $this->dataset;
    }
}