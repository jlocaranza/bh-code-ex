<?php


class BaseRequest
{
    public $token;
    public $custId;
    public $prodName;
    public $domain;
    public $startDt;
    public $length;

    public function __construct($payload)
    {
        $this->token = $payload['token'];
        $this->custId = $payload['custId'];
        $this->prodName = $payload['prodName'];
        $this->domain = $payload['domain'];
        $this->startDt = DateTime::createFromFormat('m/d/Y',$payload['startDt']);
        $this->length = $payload['length'];
    }

    public function toArray()
    {
        return [$this->custId, $this->prodName, $this->domain, $this->startDt->format('Y-m-d'), $this->length];
    }
}