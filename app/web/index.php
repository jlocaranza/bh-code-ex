<?php
$startDt = (new DateTime('now'))->format('m/d/Y');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<style>
    label {
        display: inline-block;
        width: 150px;
    }
</style>
<div id="api_response"></div>
<form id="addProductForm">
    <div>
        <label for="custId">Customer id</label>
        <input type="text" name="custId" id="custId">
    </div>
    <div>
        <label for="prodName">Product Name</label>
        <select name="prodName" id="prodName">
            <option value="domain">Domain</option>
            <option value="hosting">Hosting</option>
            <option value="pdomain">Protected Domain</option>
        </select>
    </div>
    <div>
        <label for="domain">Domain</label>
        <input type="text" name="domain" id="domain">
    </div>
    <div>
        <label for="startDt">StartDt</label>
        <input type="text" name="startDt" id="startDt" value="<?= $startDt ?>">
    </div>
    <div>
        <label for="length">Contract Length</label>
        <input type="text" name="length" id="length">
    </div>
    <div>
        <button id="addProduct" type="button">Add Product</button>
        <button id="getAll" type="button">Get All Products</button>
        <button id="listEmails" type="button">Email Schedules</button>
    </div>
</form>
<div>
    <div>Customer products</div>
    <table id="customerProducts">
        <thead>
        <tr>
            <th>CustId</th>
            <th>ProdName</th>
            <th>Domain</th>
            <th>StartDt</th>
            <th>Contract Length</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<div>
    <div>Email Schedule</div>
    <table id="emailSchedule">
        <thead>
        <tr>
            <th>CustomerID</th>
            <th>ProductName</th>
            <th>Domain</th>
            <th>EmailDate</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
</body>
<script type="application/javascript">
    $(document).ready(function () {
        $('button#addProduct').on('click', function () {
            let formData = $('#addProductForm').serialize();
            $('#api_response').html('');
            $.post('/api/?addProduct', formData).done((data) => {
                if (!data.success) {
                    $('#api_response').html("Error: " + data.msg);
                } else {
                    $('#api_response').html("Success: " + data.msg);
                }
            });
        });

        $('button#getAll').on('click', function () {
            $('#api_response').html('');
            $.post('/api/?getAll').done((data) => {
                if (!data.success) {
                    $('#api_response').html("Error: " + data.msg);
                    return;
                }
                $('#customerProducts > tbody').html('');
                let tbody = $('#customerProducts > tbody');
                $.each(data.data, (idx,item) => {
                    let tr = $('<tr>');
                    let link = $('<button>')
                        .attr('class','deleteRecord')
                        .attr('type','button')
                        .attr('data-delete', 'custId=' + item[0] + '&prodName=' + item[1] + '&domain=' + item[2])
                        .attr('title', 'Remove Product')
                        .on('click', function(event) {
                            let qrystring = $(event.currentTarget).data('delete');
                            $.get('/api/?removeProduct&'+qrystring).done((data) => {
                                if (!data.success) {
                                    $('#api_response').html("Error: " + data.msg);
                                } else {
                                    $('#api_response').html("Success: " + data.msg);
                                }
                            });
                        })
                        .text('Remove Product');
                    tr.append($('<td>').html(item[0]))
                        .append($('<td>').html(item[1]))
                        .append($('<td>').html(item[2]))
                        .append($('<td>').html(item[3]))
                        .append($('<td>').html(item[4]))
                        .append($('<td>').append(link));
                    tbody.append(tr);
                });
            });
        });

        $('button#listEmails').on('click', function () {
            $('#api_response').html('');
            $.post('/api/?listEmails').done((data) => {
                if (!data.success) {
                    $('#api_response').html("Error: " + data.msg);
                    return;
                }
                $('#emailSchedule > tbody').html('');
                let tbody = $('#emailSchedule > tbody');
                $.each(data.data, (idx,item) => {
                    let tr = $('<tr>');
                    tr.append($('<td>').html(item.custId))
                        .append($('<td>').html(item.prodName))
                        .append($('<td>').html(item.domain))
                        .append($('<td>').html(item.emailDt));
                    tbody.append(tr);
                });
            });
        });

    });
</script>
</html>




